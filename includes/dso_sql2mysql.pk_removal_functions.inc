<?php

$default_fields = [
  '__pk',
  '__unique_names',
];

/**
 * @File removes the primary keys
 */

/**
 * Removes the views sqlsrv columns
 *
 * @return mixed
 */
function dso_sql2mysql_pk_removal__views() {
    $columns = dso_sql2mysql_pk_get_columns();

    return dso_sql2mysql_pk_remove_pk_columns('views_view', $columns);
}

/**
 * Removes the user sqlsrv columns
 *
 * @return mixed
 */
function dso_sql2mysql_pk_removal__user() {
    $columns = dso_sql2mysql_pk_get_columns(['__pk2']);

    return dso_sql2mysql_pk_remove_pk_columns('users', $columns);
}

/**
 * Removes the filter_format sqlsrv columns
 *
 * @return mixed
 */
function dso_sql2mysql_pk_removal__filter_format() {
    $columns = dso_sql2mysql_pk_get_columns();

    return dso_sql2mysql_pk_remove_pk_columns('filter_format', $columns);
}

/**
 * Removes the block sqlsrv columns
 *
 * @return mixed
 */
function dso_sql2mysql_pk_removal__block() {
    $columns = dso_sql2mysql_pk_get_columns(['__unique_tmd']);

    return dso_sql2mysql_pk_remove_pk_columns('block', $columns);
}

/**
 * Drops the specified columns in the database table
 *
 * @param string $table
 *      Table name to drop the columns from
 * @param string[] $columns
 *
 * @return bool
 * @throws \DrupalUpdateException
 */
function dso_sql2mysql_pk_remove_pk_columns($table, $columns) {
    foreach ($columns as $column_name) {
        if(db_field_exists($table, $column_name)) {
            if(db_drop_field($table, $column_name) == FALSE) {
                throw new DrupalUpdateException(t("Unable to drop column @column on table @table", [
                  '@column' => $column_name,
                  '@table' => $table,
                ]));
                break;
            }
        }
    }

    return TRUE;
}

/**
 * @param array $extra_columns
 *
 * @return array
 */
function dso_sql2mysql_pk_get_columns($extra_columns = []) {
    $default_columns = [
      '__pk',
      '__unique_name',
    ];

    return array_merge($default_columns, $extra_columns);
}